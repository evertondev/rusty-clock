#Rusty-Clock

A simple terminal clock to run on the terminal.

![running the clock program on the terminal](/doc/rusty-clock.gif "Executing the program")

Thanks and credits to [Andy Thomason](https://www.youtube.com/watch?v=gX6EFBICIcY)